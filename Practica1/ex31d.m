function ex31d()
    % crea una imatge de 256 x 256 amb tot 1 double
    imgDouble = ones(256,256,1);
    % crea una imatge de 256 x 256 amb tot 1 tipus 8?bit unsigned
    imgUint8 = uint8(ones(256,256,1));
    
    % mostra les imatges
    subplot(1,2,1)
    imshow(imgDouble);
    subplot(1,2,2)
    imshow(imgUint8);
    
%     El color de les 2 imatges �s diferent ja que en el tipus 8?bit
%     unsigned els valors van de 0 a 255 sent 0 el negre i 255 el blanc,
%     per tan l'1 al ser un valor molt petit el veiem negre. 
%     En canvi en el tipus double els valors van de 0 a 1 sent 0 negre i 1 blanc
%     
%     Per poder visualitzar la imatge uint8 de color blanc el que podem fer es
%     afegir double() dins de imshow quedant aixi imshow(double(imgUint8))
end