function ex31abc()
    % crea una imatge de 256 x 256 negra
    img = uint8(zeros(256,256,1));
    % mostra per pantalla el tipus d'imatge
    display('Tipus d''imatge: ');
    class(img)
    % mostra per pantalla la mida de la imatge
    display('Tamany: ');
    size(img)
    % mostra la imatge
    imshow(img);
    % guarda la imatge a jpg
    imwrite(img, 'void_img.jpg');
end