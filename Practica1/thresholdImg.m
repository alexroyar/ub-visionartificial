function thresholdImg(Th)
    % carrega la imatge
    imCar = imread('imatges/car_gray.jpg');
    imBin = imCar;
    % posa els pixels amb valor inferior a Th a 0
    imBin(imBin < Th) = 0;
    % posa els pixels amb valor superior o igual a Th a 1
    imBin(imBin >= Th) = 1;
    
    % mostra el resultat per pantalla
    subplot(1,2,1)
    imshow(imCar);
    subplot(1,2,2)
    imshow(double(imBin));
    
    % guarda les dues imatges per ser exportades
    imOut = [imCar,double(imBin)*255];
    % exporta les imatges a jpg
    imwrite(imOut, 'hand_binary.jpg');
end