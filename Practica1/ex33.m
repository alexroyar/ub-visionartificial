function ex33()
    % carrega la imatge
    imOrig = imread('imatges/lena.jpg');
    % passa la imatge a escala de grisos
    imGray = rgb2gray(imOrig);
    
    % crea una imatge negra de de la mateixa amplada que la imatge original
    % i d'un 10% de llardaga
    bSup = uint8(zeros(size(imGray,1)*0.1,size(imGray,2)));
    imFi = vertcat(bSup, imGray); % concatena la part superior
    imFi = vertcat(imFi, bSup); % concatena la part inferior
    
    % crea una imatge negra de de la mateixa llargada que la imatge
    % resultant d'afegir-hi la part superior i inferior i d'un 10% d'amplada
    bLat = uint8(zeros(size(imFi,1),size(imGray,2)*0.1));
    imFi = horzcat(imFi, bLat); % concatena la dreta
    imFi = horzcat(bLat, imFi); % concatena l'esquerra
    % mostra la imatge resultant
    figure, imshow(imFi);
end