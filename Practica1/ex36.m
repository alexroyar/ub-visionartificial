function ex36()
    imCerc = imread('imatges/circles.bmp');
    imCerc = rgb2gray(imCerc);
    
    imDreta = double(imCerc)/255; % passa a double per facilitar la binaritzacio
    
    % posa negre els pixels mes foscos de 150
    imDreta(imDreta < double(150)/255) = 0;
    % posa a negre els pixels mes clars que 200
    imDreta(imDreta > double(200)/255) = 0;
    % posa els pixels que no son negres a blanc
    imDreta(imDreta > 0) = 1;
    
    imCentre = double(imCerc)/255;
    % posa a negre tots els pixels mes clars de 150
    imCentre(imCentre > double(150)/255) = 0;
    % posa els pixels que no son negres a blanc
    imCentre(imCentre > 0) = 1;
    
    imEsquerra = imCerc;
    % posa a blanc els pixels mes clars de 100
    imEsquerra(imEsquerra > 100) = 255;
    % fa l'invers per tal que quedi el fons negre i el cercle blanc
    imEsquerra = ~imEsquerra;
    
    figure
    subplot(2,3,1)
    imshow(imEsquerra);
    subplot(2,3,2)
    imshow(imCentre);
    subplot(2,3,3)
    imshow(imDreta);
    
    % fa l'invers per tal que quedi el fons negre i el cercle blanc
    % i aix� tenim el resultat demanat
    imEsquerraBlanc = ~imEsquerra;
    
    % posa a blanc els pixels que son 0 al imCentre, aix� queda el cercle
    % del centre del color original i la resta negre
    imCentreBlanc = imCerc;
    imCentreBlanc(imCentre == 0) = 255;
    
    % posa a blanc els pixels que son 0 al imDreta, aix� queda el cercle
    % de la dreta del color original i la resta negre
    imDretaBlanc = imCerc;
    imDretaBlanc(imDreta == 0) = 255;
    
    subplot(2,3,4)
    imshow(imEsquerraBlanc);
    subplot(2,3,5)
    imshow(imCentreBlanc);
    subplot(2,3,6)
    imshow(imDretaBlanc);
    
    % guarda les tres imatges per ser exportades
    imOut = [imEsquerraBlanc*255,imCentreBlanc,imDretaBlanc];
    
    % exporta les imatges a jpg
    imwrite(imOut, 'three_circles.jpg');
end