function ex35a(Th)
    % carrega la imatge
    imCar = imread('imatges/car_gray.jpg');
    imBin = imCar;
    % posa els pixels amb valor inferior a 20 a 0
    imBin(imBin < 20) = 0;
    % posa els pixels amb valor superior o igual a 20 a 1
    imBin(imBin >= 20) = 1;
    
    % canviant el valor de l'umbral variar� el nombre de pixels que ser�n
    % blancs o negres, per tan la imatge quedar� m�s clara o m�s fosca
    
    % mostra el resultat per pantalla
    subplot(1,2,1)
    imshow(imCar);
    subplot(1,2,2)
    imshow(double(imBin));
end