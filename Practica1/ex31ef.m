function ex31ef()
    % crea una imatge de 256 x 256 negra
    imgBW = uint8(ones(256,256,1));
    % posa la meitat esquerra de la imatge a blanc
    imgBW(:,1:128) = 255;
    % mostra la imatge
    imshow(imgBW);
    % guarda la imatge a jpg
    imwrite(imgBW, 'edge_img.jpg');
end