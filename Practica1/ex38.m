function ex38()
    imHand = imread('imatges/hand.jpg');
    imMapfre = imread('imatges/mapfre.jpg');
    size(imHand) % les dues son de dimensi� 3
    size(imMapfre)
    
    imHandMask = rgb2gray(imHand);
    imHandMask = double(imHandMask)/255;
    
    imHandMask(imHandMask > double(18)/255) = 1;
    imHandMask(imHandMask < 1) = 0; 
    
    imBackMask = ~imHandMask;
    
    i(:,:,1) = imBackMask; 
    i(:,:,2) = imBackMask; 
    i(:,:,3) = imBackMask; 

    i2(:,:,1) = imHandMask;
    i2(:,:,2) = imHandMask;
    i2(:,:,3) = imHandMask;
    
    imFinal = imMapfre.*uint8(i) + imHand.*uint8(i2);
    
    imshow(imFinal);
    
    imwrite(imFinal, '3channels.jpg');
    
end