function ex37()
    im1 = uint8(zeros(255,255));
    im1(:,128:end) = 255; % crea la primera figura
    
    im2 = uint8(zeros(255,255));
    im2(128:end,:) = 255; % crea la segona figura
    
    im3 = uint8(zeros(255,255));
    im3(1:128,1:128) = 255; % crea la tercera figura
    
    colours(:,:,1) = im1;
    colours(:,:,2) = im2;
    colours(:,:,3) = im3; % ajunta les 3 figures en una matriu tridimensional
    figure, imshow(colours);
    
    imwrite(colours, '3channels.jpg');
    
end