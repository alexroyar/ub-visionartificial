function ex32()
    % carrega la imatge
    im = imread('imatges/hand_1C.jpg');
    % mostra els valors dels nivells de gris de la fila 200 en el rang de 
    % columnes [310, 330]
    display('Valors de gris: ');
    display(im(200,310:330));
    % Converteix la imatge de manera que resulta especular respecto al eix 
    % vertical
    im2 = flipdim(im,2);
    subplot(1,2,1), imshow(im2);
    % rota la imatge 90�
    im3 = imrotate(im,90);
    subplot(1,2,2), imshow(im3);
    % guarda les imatge a jpg
    imwrite(im2, 'hand_flip.jpg');
    imwrite(im3, 'hand_rot.jpg');
end