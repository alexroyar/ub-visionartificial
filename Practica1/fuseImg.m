function fuseImg()
    imHand = imread('imatges/hand.jpg'); 
    imMapfre = imread('imatges/mapfre.jpg'); %obre les imatges
    size(imHand) % les dues son de dimensió 3
    size(imMapfre)
    
    imHandMask = rgb2gray(imHand); % passa la imatge hand a escala de grisos
    imHandMask = double(imHandMask)/255; % passa la imatge a double per facilitar
                                         % la binarització
    
    imHandMask(imHandMask > double(18)/255) = 1; % posa a blanc els pixels clars
    imHandMask(imHandMask < 1) = 0; % posa a negre els pixels que no son blancs
    
    imBackMask = ~imHandMask; % guardem la mascara invertida
    
    i(:,:,1) = imBackMask; % guarda la mascara en una matriu de 3 dimensions
    i(:,:,2) = imBackMask; % per tal de poder operar amb les imatges
    i(:,:,3) = imBackMask; 

    i2(:,:,1) = imHandMask;
    i2(:,:,2) = imHandMask;
    i2(:,:,3) = imHandMask;
    
    % aplica les mascares a cada imatge i les suma, per ajuntar-les
    imFinal = imMapfre.*uint8(i) + imHand.*uint8(i2);
    
    figure, imshow(imFinal);
    
    %guarda la imatge final a un arxiu jpg
    imwrite(imFinal, 'hand_mapfre_3C.jpg');
    
end