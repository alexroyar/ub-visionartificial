function ex34()
    % carrega la imatge
    imCar = imread('imatges/car_lowContrast.jpg');
    % suma a tots els pixels la difer�ncia entre 255 i el valor del pixel
    % m�s clar
    imCarBright = imCar+(255-max(imCar(:)));
    % guarda la imatge a jpg
    imwrite(imCarBright, 'car_bright.jpg');
    % mostra la imatge
    subplot(1,3,1)
    imshow(imCarBright);
    % resta a tots els pixels el valor del pixel m�s fosc
    imCarDark = imCar-min(imCar(:));
    % guarda la imatge a jpg
    imwrite(imCarDark, 'car_Dark.jpg');
    % mostra la imatge
    subplot(1,3,2)
    imshow(imCarDark);
    
    % passa la imatge a double
    imCarDouble = double(imCar)/255;
    % augmenta el contrast de la imatge restant el valor del pixel m�s fosc
    % a tots els pixels de la imatge i dividint el resultat per la
    % diferencia entre el valor del pixel m�s clar i el valor del pixel m�s
    % fosc
    imCarContrast = (imCarDouble-double(min(imCarDouble(:)))) / double((max(imCarDouble(:)))-double(min(imCarDouble(:))));
    
    % guarda la imatge a jpg
    imwrite(imCarContrast, 'car_contrast.jpg');
    % mostra la imatge
    subplot(1,3,3)
    imshow(imCarContrast);
end