% a)
[m,s] = showMatches('Starbucks.jpg', 'starbucks6.jpg');

% b)
[m,s] = showMatches('Starbucks.jpg', 'starbucks2.png');
[m,s] = showMatches('Starbucks.jpg', 'starbucks4.jpg');
[m,s] = showMatches('Starbucks.jpg', 'starbucks5.png');

% c)
[m6,s] = showMatches('Starbucks.jpg', 'cola2.jpg');

% e) 
[m6,s] = showMatches('cola2.jpg', 'cola1.jpeg');
