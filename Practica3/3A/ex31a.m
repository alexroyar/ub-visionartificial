function []=ex31a()

% La funcion miTestKmeans() ilustra la posibilidad del m�todo k-means de encontrar agrupacions (cl�sters) de puntos. 

% Generamos tres nubes de 100 puntos con una distribuci�n Gaussiana, usando la funci�n gaussRandom proporcionada con el enunciado, utilizando como centro las coordenadas [1 2], [2 2] y [2 1] y desviaci�n est�ndar de 0.1 en todos los ejes. 

c1=gaussRandom([1 2], 0.1 ,100);
c2=gaussRandom([2 2], 0.1 ,100);    
c3=gaussRandom([2 1], 0.1 ,100);

%% b) Plotea los puntos con colores diferentes 
close all;
subplot(2,2,1:2);
plot(c1(:,1), c1(:,2),'r+')
hold on;
plot(c2(:,1), c2(:,2),'g+')
plot(c3(:,1), c3(:,2),'b+')
hold off;
title('Initial distribution');

%% c) Usa Kmeans para clusterizar los datos usando k=2,3
X=[c1;c2;c3];
plotFlag=0;

bandWidth=0.3;
[ctrs2,idx2] = MeanShiftCluster(X',bandWidth,plotFlag);
bandWidth=0.5;
[ctrs4,idx4] = MeanShiftCluster(X',bandWidth,plotFlag);
bandWidth=0.75;
[ctrs6,idx6] = MeanShiftCluster(X',bandWidth,plotFlag);


% [idx2,ctrs2] = kmeans(X,2);
% [idx3,ctrs3] = kmeans(X,3);
% ....................................Apply kmeans with k=4

% Visualiza los datos originales en la primera fila, y el resultado (incluyendo los centros) de las agrupaciones con 2 y 3 centros respectivamente utilizando diferentes colores.

% Show final results
cVec='bgrcmykbgrcmykbgrcmykbgrcmykbgrcmykbgrcmykbgrcmyk';
subplot(2,3,4);
for i=1: size(ctrs2,2)
    plot(X(idx2==i,1),X(idx2==i,2),[cVec(i) '.'],'MarkerSize',12)
    hold on;
    plot(ctrs2(1,i),ctrs2(2,i),'ko','MarkerSize',12,'LineWidth',2)
end
hold off;
title('Bandwith = 0.3');

subplot(2,3,5);
for i=1: size(ctrs4,2)
    plot(X(idx4==i,1),X(idx4==i,2),[cVec(i) '.'],'MarkerSize',12)
    hold on;
    plot(ctrs4(1,i),ctrs4(2,i),'ko','MarkerSize',12,'LineWidth',2)
end
hold off;
title('Bandwith = 0.5');

subplot(2,3,6);
for i=1: size(ctrs6,2)
    plot(X(idx6==i,1),X(idx6==i,2),[cVec(i) '.'],'MarkerSize',12)
    hold on;
    plot(ctrs6(1,i),ctrs6(2,i),'ko','MarkerSize',12,'LineWidth',2)
end
hold off;
title('Bandwith = 0.75');

end

