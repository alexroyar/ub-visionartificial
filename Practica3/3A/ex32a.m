function []=ex32a()
    im = imread('loro.png');
    R = im(:,:,1);
    G = im(:,:,2);
    B = im(:,:,3);
    X(:,1) = R(:);
    X(:,2) = G(:);
    X(:,3) = B(:);
    [i,c] = kmeans(double(X),8);
    c = c(i,:);
    i = zeros(300,400,1);
    i(:,:,1) = reshape(c(:,1), [300,400]);
    i(:,:,2) = reshape(c(:,2), [300,400]);
    i(:,:,3) = reshape(c(:,3), [300,400]);
    subplot(2,2,1), imshow(im)
    subplot(2,2,2), plotPixelDistribution(im)
    subplot(2,2,3), imshow(uint8(i))
    subplot(2,2,4), plotPixelDistribution(i)

end