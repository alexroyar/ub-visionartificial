function []=ex31b()
% La funcion miTestKmeans() ilustra la posibilidad del m?todo k-means de encontrar agrupacions (cl?sters) de puntos. 

% Generamos tres nubes de 100 puntos con una distribuci?n Gaussiana, usando la funci?n gaussRandom proporcionada con el enunciado, utilizando como centro las coordenadas [1 2], [2 2] y [2 1] y desviaci?n est?ndar de 0.1 en todos los ejes. 

c1=gaussRandom([1 2], 0.1 ,100);
c2=gaussRandom([2 2], 0.1 ,100);    
c3=gaussRandom([2 1], 0.1 ,100);

%% b) Plotea los puntos con colores diferentes 
close all;
subplot(2,2,1:2);
plot(c1(:,1), c1(:,2),'r+')
hold on;
plot(c2(:,1), c2(:,2),'g+')
plot(c3(:,1), c3(:,2),'b+')
hold off;
title('Initial distribution');

%% c) Usa Kmeans para clusterizar los datos usando k=2,3
X=[c1;c2;c3];

[idx2,ctrs2] = kmeans(X,2);
[idx3,ctrs3] = kmeans(X,3);
[idx4,ctrs4] = kmeans(X,4);

% Visualiza los datos originales en la primera fila, y el resultado (incluyendo los centros) de las agrupaciones con 2 y 3 centros respectivamente utilizando diferentes colores.

% Show final results
cVec='bgrcmykbgrcmykbgrcmykbgrcmykbgrcmykbgrcmykbgrcmyk';
subplot(2,3,4);
for i=1: size(ctrs2,1)
    plot(X(idx2==i,1),X(idx2==i,2),[cVec(i) '.'],'MarkerSize',12)
    hold on;
    plot(ctrs2(i,1),ctrs2(i,2),'ko','MarkerSize',12,'LineWidth',2)
end
hold off;
title('K-means K = 2');

subplot(2,3,5);
for i=1: size(ctrs3,1)
    plot(X(idx3==i,1),X(idx3==i,2),[cVec(i) '.'],'MarkerSize',12)
    hold on;
    plot(ctrs3(i,1),ctrs3(i,2),'ko','MarkerSize',12,'LineWidth',2)
end
hold off;
title('K-means K = 3');

subplot(2,3,6);
for i=1: size(ctrs4,1)
    plot(X(idx4==i,1),X(idx4==i,2),[cVec(i) '.'],'MarkerSize',12)
    hold on;
    plot(ctrs4(i,1),ctrs4(i,2),'ko','MarkerSize',12,'LineWidth',2)
end
hold off;
title('K-means K = 4');

end

