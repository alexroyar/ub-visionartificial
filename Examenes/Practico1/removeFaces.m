function [ outImg ] = removeFaces( img )
%REMOVEFACES Remove faces from input image
%   Fill the steps in order to remove the faces

    % b) Get mask with skin pixels
    skinPixels=imageThr(img,[115 255;0 170;0 150]);
    % c) Improve the mask with median filter
    faceMask=medfilt2(skinPixels);
    
    % d) Smooth the image with a gaussian filter
    smoothImage=imgaussian(img,2);
      
    % e) Cut the face region from smoted image
    class(smoothImage(:,:,1))
    
         % multiplico la imatge borrosa per la m�scara per tal de quedar-me
         % nom�s amb la part desitjada
    smoothFace(:,:,1) = smoothImage(:,:,1).*uint8(faceMask);
    smoothFace(:,:,2) = smoothImage(:,:,2).*uint8(faceMask);
    smoothFace(:,:,3) = smoothImage(:,:,3).*uint8(faceMask);
    
    % f) Create the final image using the original image and the smoothed face

            % multiplico la imatge original per la m�scara inversa per tal
            % de posar a 0 la part desitjada (la cara)
    outImage(:,:,1) = (img(:,:,1)).*uint8(~faceMask);
    outImage(:,:,2) = img(:,:,2).*uint8(~faceMask);
    outImage(:,:,3) = img(:,:,3).*uint8(~faceMask);
            
        % sumo a la imatge original la part borrosa extreta anteriorment i
        % ja tenim el resultat final.
    outImage=outImage+smoothFace;
    
    % Show the face region and final image
    close all;
    subplot(2,3,1);imshow(img);title('Input image');
    subplot(2,3,2);imshow(skinPixels);title('Skin pixels');
    subplot(2,3,3);imshow(faceMask);title('Face mask');
    subplot(2,3,4);imshow(smoothImage);title('Smoothed image');
    subplot(2,3,5);imshow(smoothFace);title('Smoothed face');
    subplot(2,3,6);imshow(outImage);title('Anonimized image');
end

