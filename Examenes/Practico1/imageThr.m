function [outImg] = imageThr(img, rangMat)

    img = double(img)/255;
    % filtro el vermell pels paramatres passats
    R = img(:,:,1);
    R(R > double(rangMat(1,1))/255) = 1;
    R(R < double(rangMat(1,2))/255) = 0;
    % filtro el verd pels paramatres passats
    G = img(:,:,2);
    G(G > double(rangMat(2,1))/255) = 1;
    G(G < double(rangMat(2,2))/255) = 0;
    % filtro el blau pels paramatres passats
    B = img(:,:,3);
    B(B > double(rangMat(3,1))/255) = 1;
    B(B < double(rangMat(3,2))/255) = 0;
    
    %guardo la suma dels filtrats ja que volem una imatge d'una sola
    %dimensi�
    im = ~R+~G+~B;
    
    % inverteixo la imatge per obtenir el resultat esperat
    outImg = ~im;
    
end