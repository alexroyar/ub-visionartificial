% Jaume S�nchez Castellanas, Grupo B
function SanchezCastellanasJaumeEjercicio2()

    img = imread('friends.jpg');
    imgB = rgb2gray(img);
    % aplico el filtre Laplacian
    h=fspecial('laplacian',0.1); 
    imLaplacian = imfilter(imgB, h);
    
    % Passo el contorn a vermell
    sub(:,:,1) = imLaplacian;
    sub(:,:,2) = sub(:,:,1)*0;
    sub(:,:,3) = sub(:,:,1)*0;
    % Trec el contorn a la imatge original
    imgFi(:,:,1) = img(:,:,1).*uint8(~imLaplacian);
    imgFi(:,:,2) = img(:,:,2).*uint8(~imLaplacian);
    imgFi(:,:,3) = img(:,:,3).*uint8(~imLaplacian);
    % afegeixo el contorn vermell a la imatge original
    imgFi = img+sub;
    
    subplot(1,3,1); imshow(img); title('original');
    subplot(1,3,2); imshow(imLaplacian); title('Laplacian edge');
    subplot(1,3,3); imshow(imgFi); title('original + red laplacian edge');

end