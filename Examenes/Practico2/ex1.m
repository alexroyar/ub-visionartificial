% Jaume S�nchez Castellanas 

function ex1()
    im = imread('corals.jpg');
    out = reduceNumColors(im);
    figure,
    subplot(1,2,1),imshow(im), title('Original');
    subplot(1,2,2),imshow(out), title('K = 16, 4 bits');
end