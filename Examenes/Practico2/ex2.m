% Jaume S�nchez Castellanas

function ex2()
    im1 = imread('KFCLogo1.jpg');
    im2 = imread('KFCLogo2.jpg');
    im3 = imread('KFCLogo3.jpg');
    
    % entero que servir� para indentificar la imagen con mas parecido a la
    % escena.
    masParecido = 1;
    
    % aplico showMatches a las 2 primeras imagenes
    figure,
    subplot(4,1,2),[m,s] = showMatches('KFCLogo1.jpg', 'streetImage.jpg'), 
    title('SWIFT Matches for logo 1');
    subplot(4,1,3),[m2,s2] = showMatches('KFCLogo2.jpg', 'streetImage.jpg'), 
    title('SWIFT Matches for logo 2');
    
    % compruebo si la segunda imagen tiene m�s semejanzas que la primera,
    % en caso que si pongo masParecido a 2 i el valor de s igual a s2
    if (sum(s2) > sum(s)) 
        masParecido = 2;
        s = s2;
    end
    
    subplot(4,1,4),[m3,s3] = showMatches('KFCLogo3.jpg', 'streetImage.jpg'), 
    title('SWIFT Matches for logo 3');
    % compruebo si la tercera imagen es mas parecida que la mas parecida de
    % las dos anteriores, en caso de serlo pongo masParecido a 3
    if (sum(s3) > sum(s))
        masParecido = 3;
    end
    
    % muestro en la primera posicion la imagen mas parecida teniendo en
    % cuenta el entero masParecido.
    switch masParecido
        case 1
            subplot(4,1,1),imshow(im1),title('Most similar logo');
        case 2
            subplot(4,1,1),imshow(im2),title('Most similar logo');
        case 3
            subplot(4,1,1),imshow(im3),title('Most similar logo');
    end
end