% Jaume S�nchez Castellanas

function [out] = reduceNumColors(im)
    % convierto la imagen en un vector de 3 dimensiones (1 por cada color RGB)
    % que utilizar� en el kmeans
    R = im(:,:,1);
    G = im(:,:,2);
    B = im(:,:,3);
    X(:,1) = R(:);
    X(:,2) = G(:);
    X(:,3) = B(:);
    % utilizo 4 bits, por tanto 16 colores
    % entiendo que se pide solo una imagen y no las 3 con diferentes bits
    % ya que pide una imagen de entrada y otra de salida.
    % Por eso he elegido 4 bits, que es la que pone en las notas.
    % En caso de querer cambiar el numero de colores solo hay que cambiar
    % el 16 del kmeans por 8 o 32, si queremos 3 o 5 bits.
    [i,c] = kmeans(double(X),16);
    c = c(i,:);
    [h,w,d] = size(im);
    % transformo el vector a matriz para tener la imagen de salida.
    out = zeros(h,w,d);
    out(:,:,1) = reshape(c(:,1), [h,w]);
    out(:,:,2) = reshape(c(:,2), [h,w]);
    out(:,:,3) = reshape(c(:,3), [h,w]);
    out = uint8(out);
end