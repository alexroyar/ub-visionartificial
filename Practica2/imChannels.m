function [  ] = imChannels(file)
%This function reads a color image and displays its channels
 
im=imread(file);
imR = im(:,:,1) % define the Red channel 
imG = im(:,:,2) % define the Green channel 
imB = im(:,:,3) % define the Blue channel 

figure, subplot(2,3,2), imshow(im),...
    subplot(2,3,4), imshow(imR),...
    subplot(2,3,5), imshow(imG),...
    subplot(2,3,6), imshow(imB);

im(:,:,1) = 0;
figure, imshow(im);
% suprimint un canal estem eliminant el seu color en la imatge

im(:,:,1) = imG;
im(:,:,2) = imB;
im(:,:,3) = imR;

figure, imshow(im);
% intercanviant els canals canvien els colors de la imatge

    
end
