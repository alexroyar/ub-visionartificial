function [] = ej22(file) % file is the name of the image
%ej22 This function illustrate the effect of resizing and smoothing with different 1D and 2D masks 
 
% clear all;
close all;
 
im=imread(file); % e.g. im=imread('./images/cruis.jpg');
 
imGray=rgb2gray(im);
resizedIm=imresize(imGray, 2);

h=[1 1 1 1 1]; % 1D mask defined
h=h/sum(h); % normalization
 
h2= [1, 1, 1, 1, 1];
h2=h2/sum(h2);

h3= [[1 1 1 1 1]; [1 1 1 1 1]; [1 1 1 1 1]; [1 1 1 1 1]; [1 1 1 1 1]];
h3=h3/sum(h3);

% define the 2D mask, shell we normalize it?
% Yes, we have to normalize the 2D mask too
% .......
 
smoothedImGray=conv2(double(imGray),h);
smoothedImGray2=conv2(double(imGray),h2);
smoothedImGray3=conv2(double(imGray),h3);
smoothedImGray4=conv2(double(smoothedImGray3),h3);
smoothedImGray5=conv2(double(smoothedImGray4),h3);

smoothedImGrayMed = medfilt2(imGray);

figure, subplot(4,2,1),imshow(uint8(smoothedImGray)),title('convolution mask [1 1 1 1 1]'),...
    subplot(4,2,3), imshow(uint8(smoothedImGray2)),title('convolution mask [1, 1, 1, 1, 1]'),...
    subplot(4,2,5), imshow(uint8(smoothedImGray3)),title('convolution mask [[1 1 1 1 1]; [1 1 1 1 1]; [1 1 1 1 1]; [1 1 1 1 1]; [1 1 1 1 1]]'),
    subplot(4,2,7), imshow(uint8(smoothedImGray5)),title('convolucio de l''anterior dos vegades'),
    subplot(4,2,4), imshow(uint8(smoothedImGrayMed)),title('filtre de mediana');
    
% veiem que com mes s'aplica el filtre la imatge queda m�s difuminada


% proves per comprovar si es pot aplicar el filtre a la imatge en color
smoothedImGrayRGB(:,:,1)= conv2(double(im(:,:,1)),h);
smoothedImGrayRGB(:,:,2)= conv2(double(im(:,:,2)),h);
smoothedImGrayRGB(:,:,3)= conv2(double(im(:,:,3)),h);


 
figure, subplot(3,3,2), imshow(im, []), ...
    subplot(3,3,4), imshow(imGray),  ...
    subplot(3,3,5), imshow(resizedIm),...
    subplot(3,3,6), imshow(uint8(smoothedImGray)),...
    subplot(3,3,7), imhist(imGray),  ...
    subplot(3,3,8), imhist(resizedIm),...
    subplot(3,3,9), imhist(uint8(smoothedImGray));
  
% add more figures if necessary
end
