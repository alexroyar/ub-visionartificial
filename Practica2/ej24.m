function [] = ej24()
    im=imread('imatges/starbuck.jpg');
    %aplico el filtre laplacian a la imatge
    h=fspecial('laplacian',0.2); 
    % sumo la imatge original amb la que te el filtre
    imLaplacian=imfilter(prova,h); 
    
    
    LaplacianSup = im+imLaplacian;
    
    %aplico el filtre Sobel a la imatge
    My = fspecial('sobel');
    imSobel = imfilter(im, My); 
    % sumo la imatge original amb la que te el filtre
    SobelSup = im+imSobel;
    
    figure,
    subplot(2,3,1), imshow(im),title('original'),
    subplot(2,3,2), imshow(imLaplacian), title('Laplacian');
    subplot(2,3,3), imshow(LaplacianSup), title('superposiciˇ');
    subplot(2,3,4), imshow(im),title('original'),
    subplot(2,3,5), imshow(imSobel), title('Sobel');
    subplot(2,3,6), imshow(SobelSup), title('superposiciˇ');

    im=imread('imatges/bebe.jpg');
    
    %aplico el filtre laplacian a la imatge
    h=fspecial('laplacian',0.2); 
    % sumo la imatge original amb la que te el filtre
    imLaplacian2=imfilter(im,h);
    
    im=imread('imatges/iwum.jpg');
    
    %aplico el filtre laplacian a la imatge
    h=fspecial('laplacian',0.2); 
    % sumo la imatge original amb la que te el filtre
    imLaplacian3=imfilter(im,h);
    
    
    im=imread('imatges/halloween.jpg');
    
    %aplico el filtre laplacian a la imatge
    h=fspecial('laplacian',0.2); 
    % sumo la imatge original amb la que te el filtre
    imLaplacian4=imfilter(im,h);
    
    figure, imshow(imLaplacian4)
    
end